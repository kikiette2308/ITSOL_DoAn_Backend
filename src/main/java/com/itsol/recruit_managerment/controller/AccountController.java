package com.itsol.recruit_managerment.controller;

import com.itsol.recruit_managerment.dto.UserSignupDTO;
import com.itsol.recruit_managerment.email.EmailServiceImpl;
import com.itsol.recruit_managerment.model.OTP;
import com.itsol.recruit_managerment.model.User;
import com.itsol.recruit_managerment.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;

@RestController
@RequiredArgsConstructor
public class AccountController {
@Autowired
  UserService userService;
@Autowired
    PasswordEncoder passwordEncoder;
    private final EmailServiceImpl emailService;

    @PostMapping("/signup")
    public ResponseEntity<String> signup(@RequestBody UserSignupDTO userSignupDTO){
        User user = new User();
        user.setFullName(userSignupDTO.getFullName());
        user.setEmail(userSignupDTO.getEmail());
        user.setPhoneNumber(userSignupDTO.getPhoneNumber());
        user.setHomeTown(userSignupDTO.getHomeTown());
        user.setGender(userSignupDTO.getGender());
        user.setUserName(userSignupDTO.getUserName());
        user.setPassword(passwordEncoder.encode(userSignupDTO.getPassword()));
        user.setRoles(userSignupDTO.getRoles());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            user.setBirthDay(sdf.parse(userSignupDTO.getBirthDay()));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        user.setActive(false);
        user.setIsDelete(0); //  0 => deleted = false
        userService.saveUser(user);
        OTP otp = userService.generateOTP(user);
        String linkActive = "http://localhost:8080/active/" + user.getId();
        emailService.sendSimpleMessage(user.getUserName(),
                                "OTP active account",
                                   "OTP: " + otp.getCode() + "\nLink activate: " + linkActive);
        return ResponseEntity.ok().body("check email for OTP");
    }

    @PostMapping("/active/{id}")
    public ResponseEntity<String> activeAccount(@PathVariable Long id, @RequestParam String otpCode){
        try {
            User user = userService.getUserById(id);
            if(user.isActive()){
                return ResponseEntity.ok().body("Your account is already activated");
            }
            OTP otp = userService.getOTPByUser(user);
            userService.verifyOTP(otp, otpCode);
            userService.activeAccount(user);
            return ResponseEntity.ok().body("Active successfull");
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
